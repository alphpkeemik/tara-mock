<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;
/**
 * @ORM\Entity()
 */
class Token
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json_array")
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sub;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $uid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $stamp;

    public function __construct(string $sub, array $data)
    {
        $this->data = $data;
        $this->sub = $sub;
        $this->uid = substr(uniqid() . hash('sha256', sprintf('%s-%s-%s', microtime(), $sub, json_encode($data))), 0, 36);
        $this->stamp = new DateTime();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getData(): array
    {
        return $this->data;
    }


    public function getSub(): string
    {
        return $this->sub;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getStamp(): ?DateTimeInterface
    {
        return $this->stamp;
    }

    public function getExpire(): ?DateTimeInterface
    {
        $clone = clone $this->getStamp();
        $clone->format('+1 day');

        return $clone;
    }


}
