<?php
namespace App\Controller;

use App\Entity\Token;
use App\Form\AuthorizeResponseType;
use App\Form\AuthorizeType;
use App\Form\TokenType;
use Gamegos\JWS\JWS;
use Jose\Component\Core\JWK;
use Jose\Component\Core\Util\RSAKey;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @author mati@we.ee
 * @see    https://e-gov.github.io/TARA-Doku/TehnilineKirjeldus
 */
class AuthorizeController extends AbstractController
{

    CONST SESSION_DATA_KEY = 'fc63f92f-a584-495a-bac7-5381fe0dddfe';

    private function getRequestForm(Request $request): FormInterface
    {
        $requestForm = $this->createForm(AuthorizeType::class);
        $requestForm->submit($request->query->all());
        if (!$requestForm->isValid()) {
            throw new BadRequestHttpException(
                (string )$requestForm->getErrors(true, false)
            );
        }

        return $requestForm;

    }

    public function authorize(Request $request): Response
    {

        $this->getRequestForm($request)->getData();
        $data = null;
        $session = $this->container->get('session');
        if ($session->has(static::SESSION_DATA_KEY)) {
            $data = $session->get(static::SESSION_DATA_KEY);
        }


        $form = $this->createForm(AuthorizeResponseType::class, $data);

        return $this->render('authorize.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function authorizeSubmit(Request $request): Response
    {

        $rd = $this->getRequestForm($request)->getData();

        $form = $this->createForm(AuthorizeResponseType::class);
        $form->handlerequest($request);
        if (!$form->isValid()) {
            return $this->$form('authorize.html.twig', [
                'form' => $form->createView()
            ]);
        }

        $d = $form->getData();
        $parts = [
            'state' => $rd['state']
        ];
        if ($d['type'] === 'success') {


            $data = [];

            foreach (['given_name', 'family_name'] as $field) {
                if ($d[$field]) {
                    $data[$field] = $d[$field];
                }
            }
            if ($d['date_of_birth']) {
                $data['date_of_birth'] = $d['date_of_birth']->format('Y-m-d');
            }
            $em = $this->getDoctrine()->getManagerForClass(Token::class);

            $token = new Token(
                $d['sub-country'] . $d['sub-id'], $data

            );
            $em->persist($token);
            $em->flush();

            $this->container->get('session')->set(static::SESSION_DATA_KEY, $d);

            $parts['code'] = $token->getUid();
        } else {
            $parts['error'] = $d['error'];
            $parts['error_description'] = $d['error_description'];
        }

        return new RedirectResponse($rd['redirect_uri'] . '?' . http_build_query($parts));
    }

    public function token(Request $request): Response
    {


        $form = $this->createForm(TokenType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            throw new BadRequestHttpException(
                (string )$form->getErrors(true, false)
            );
        }

        $em = $this->getDoctrine()->getManagerForClass(Token::class);
        $entity = $em->getRepository(Token::class)->findOneBy([
            'uid' => $form['code']->getData()
        ]);
        if (!$entity) {
            throw new BadRequestHttpException('Token by code not found');
        }

        if ($request->headers->get('Content-Type') !== 'application/x-www-form-urlencoded') {
            throw new BadRequestHttpException('Content-Type header must be application/x-www-form-urlencoded');
        }

        if (!$request->headers->get('Authorization')) {
            return new JsonResponse([
                'headers' => $request->headers->all()
            ]);
            throw new BadRequestHttpException('Authorization header required');
        }
        if (!preg_match('/^Basic (.+)/', $request->headers->get('Authorization'), $matches)) {
            throw new BadRequestHttpException('Invalid Authorization header');
        }
        $decoded = base64_decode($matches[1], true);
        if ($decoded === false) {
            throw new BadRequestHttpException('Invalid Authorization header');
        }

        $headers = array(
            'alg' => 'HS256',
            'type' => 'JWK'
        );

        $payload = [
            'jti' => $entity->getUid(),
            'iss' => $request->getBaseUrl(),
            'exp' => $entity->getExpire()->format('U'),
            'iat' => $entity->getStamp()->format('U'),
            'nbf' => $entity->getStamp()->format('U'),
            'sub' => $entity->getSub(),
            'profile_attributes' => $entity->getData(),
            'amr' => ' we-test',
            'state' => uniqid(),
            'nonce' => uniqid(),
            'at_hash' => base64_encode($entity->getId())
        ];


        $keyArray = json_decode(file_get_contents('https://tara-test.ria.ee/oidc/jwks'), true)['keys'][0];
        $key = JWK::create($keyArray);

        $rsaKey = RSAKey::createFromJWK($key);

        $jws = new JWS();
        $encoded = $jws->encode($headers, $payload,
            $rsaKey->toPEM()
        );

        return new JsonResponse([
            'id_token' => $encoded
        ], 200, ['Access-Control-Allow-Origin' => '*']);

    }
}
