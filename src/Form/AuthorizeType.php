<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @author mati@we.ee
 */
class AuthorizeType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder->add('redirect_uri', UrlType::class, [
            'constraints' => [
                new NotBlank(),
                new Url()
            ]
        ]);

        $builder->add('scope', ChoiceType::class, [
            'multiple' => true,
            'constraints' => [
                new NotBlank()
            ],
            'choices' => [
                'OpenID' => 'openid',
                'EidasOnly' => 'eidasonly',
                'BirthName' => 'eidas:birth_name',
                'PlaceOfBirth' => 'eidas:place_of_birth',
                'CurrentAddress' => 'eidas:current_address',
                'Gender' => 'eidas:gender',
                'LegalPersonIdentifier' => 'eidas:legal_person_identifier',
                'LegalName' => 'eidas:legal_name',
                'LegalAddress' => 'eidas:legal_address',
                'VATRegistration' => 'eidas:vat_registration',
                'TaxReference' => 'eidas:tax_reference',
                'LEI' => 'eidas:lei',
                'EORI' => 'eidas:eori',
                'SEED' => 'eidas:seed',
                'SIC' => 'eidas:sic',
                'D-2012-17-EUIdentifier' => 'eidas:d-2012-17-eu_identifier',

            ]
        ]);
        $builder->get('scope')->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();

            if (!is_string($data)) {
                return;

            }
            $event->setData(explode(' ', urldecode($data)));
        });

        $builder->add('state', TextType::class, [
            'constraints' => [
                new NotBlank(),
                //https://stackoverflow.com/questions/475074/regex-to-parse-or-validate-base64-data
                new Callback(function ($payload, ExecutionContextInterface $context) {
                    if (!$payload) {
                        return;
                    }
                    $value = base64_decode($payload, true);
                    if (!$value) {
                        $context->buildViolation('Not base64 data!')
                            ->atPath('state')
                            ->addViolation();
                    }
                })
            ]
        ]);

        $builder->add('response_type', ChoiceType::class, [
            'constraints' => [
                new NotBlank()
            ],
            'choices' => [
                'OpenID Connect protocol authorization flow' => 'code'
            ]
        ]);

        $builder->add('client_id', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ]);
        $builder->add('locale', ChoiceType::class, [
            'choices' => [
                'et' => 'et',
                'en' => 'en',
                'ru' => 'ru'
            ]
        ]);
        $builder->add('nonce', TextType::class);
        $builder->add('acr_values', ChoiceType::class, [
            'choices' => [
                'low' => 'low',
                'substantial' => 'substantial',
                'high' => 'high'
            ]
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('csrf_protection', false);
    }
}