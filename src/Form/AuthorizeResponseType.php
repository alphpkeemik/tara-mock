<?php
namespace App\Form;

use App\Form\Toggle\ToggleConfig;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @author mati@we.ee
 */
class AuthorizeResponseType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $typeToggle = new ToggleConfig();
        $builder->add('type', ChoiceType::class, [
            'constraints' => [
                new NotBlank()
            ],
            'choices' => [
                'Success' => 'success',
                'Error' => 'error'
            ],
            'toggle_config' => $typeToggle
        ]);


        $builder->add('sub-country', CountryType::class, [
            'placeholder' => '',
            'label' => 'Person country',
            'constraints' => [
                new NotBlank()
            ],
            'toggle_config' => new ToggleConfig('value=="success"', [$typeToggle->createToggler()])
        ]);
        $builder->add('sub-id', TextType::class, [
            'label' => 'Person id',
            'constraints' => [
                new NotBlank()
            ],
            'toggle_config' => new ToggleConfig('value=="success"', [$typeToggle->createToggler()])
        ]);

        $builder->add('date_of_birth', DateType::class, [
            'required' => false,
            'widget' => 'single_text',
            'toggle_config' => new ToggleConfig('value=="success"', [$typeToggle->createToggler()])
        ]);
        $builder->add('given_name', TextType::class, [
            'required' => false,
            'toggle_config' => new ToggleConfig('value=="success"', [$typeToggle->createToggler()])
        ]);
        $builder->add('family_name', TextType::class, [
            'required' => false,
            'toggle_config' => new ToggleConfig('value=="success"', [$typeToggle->createToggler()])
        ]);

        $builder->add('error', TextType::class, [
            'constraints' => [
                new NotBlank()
            ],
            'toggle_config' => new ToggleConfig('value=="error"', [$typeToggle->createToggler()])
        ]);
        $builder->add('error_description', TextType::class, [
            'constraints' => [
                new NotBlank()
            ],
            'toggle_config' => new ToggleConfig('value=="error"', [$typeToggle->createToggler()])
        ]);


        $builder->add('submit', SubmitType::class);
    }
}