<?php
namespace App\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormTypeExtensionInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\ExpressionLanguage\JavascriptCompiler;
use App\Form\Toggle\ToggleConfig;

/**
 * @author mati@we.ee
 */
final class ToggleTypeExtension extends AbstractTypeExtension implements FormTypeExtensionInterface
{

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        parent::configureOptions($resolver);
        $resolver->setDefault('toggle_config', null);
        $resolver->setAllowedTypes('toggle_config', [ToggleConfig::class, 'null']);
    }

    public function getExtendedType()
    {
        return FormType::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        if (!$options['toggle_config']) {
            return;
        }
        if ($options['toggle_config']->isToggled() and $options['required']) {
            $options['toggle_config']->getTogglers()->lock();
            $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) {

                if (!$this->resolveIsVisible($event->getForm())) {
                    $event->getForm()->getParent()->remove($event->getForm()->getName());
                }
            });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        if (!$options['toggle_config']) {
            return;
        }
        if ($options['toggle_config']->isToggler()) {
            $view->vars['attr']['data-app-togglevisibility-source'] = $options['toggle_config']->getId();
        }
        if ($options['toggle_config']->isToggled()) {
            $options['toggle_config']->getTogglers()->lock();

            $compiler = new JavascriptCompiler($options['toggle_config']->getFunctions());
            $c = [];
            $c['expression'] = $compiler->compile($options['toggle_config']->getParsedExpression()->getNodes())->getSource();
            $c['togglers'] = [];
            foreach ($options['toggle_config']->getTogglers() as $item) {
                $c['togglers'][$item->getVar()] = $item->getConfig()->getId();
            }
            $view->vars['row_attr']['data-app-form-toggle_visibility'] = json_encode($c);
            if (!$this->resolveIsVisible($form)) {
                $view->vars['attr']['disabled'] = true;
                $view->vars['attr']['data-toggle_visibility_disabled'] = true;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);
        if (!($options['toggle_config'] and $options['toggle_config']->isToggled())) {
            return;
        }
        $options['toggle_config']->getTogglers()->lock();
        if (!$this->resolveIsVisible($form)) {
            $view->vars['row_attr']['data-app-togglevisibility-hidden'] = null;
        }
    }

    protected function resolveIsVisible(FormInterface $self)
    {
        $values = [];
        $csById = [];
        $sc = $self->getConfig()->getOptions()['toggle_config'];
        foreach ($sc->getTogglers() as $tc) {
            $values[$tc->getVar()] = null;
            $csById[$tc->getConfig()->getId()] = $tc;
        }
        foreach ($this->findTogglers($self) as $form) {
            $tc = $csById[$form->getConfig()->getOptions()['toggle_config']->getId()];
            $values[$tc->getVar()] = $tc->getValue($form);
        }

        return $sc->getParsedExpression()->getNodes()->evaluate($sc->getFunctions(), $values);
    }

    protected function findTogglers(FormInterface $form): array
    {
        $parent = $form;
        while ($parent->getParent()) {
            $parent = $parent->getParent();
        }
        return $this->findTogglersRecursive($parent, $form);
    }

    protected function findTogglersRecursive(FormInterface $form, FormInterface $self): array
    {
        $result = [];
        if ($form !== $self) {
            $tO = $form->getConfig()->getOptions();
            $sc = $self->getConfig()->getOptions()['toggle_config'];


            if (key_exists('toggle_config', $tO)
                and $tO['toggle_config'] instanceof ToggleConfig
                and $tO['toggle_config']->isToggler()
                and $sc->getTogglers()->hasConfig($tO['toggle_config'])) {
                $result[] = $form;
            }
        }

        foreach ($form as $child) {
            $result = array_merge($result, $this->findTogglersRecursive($child, $self));
        }


        return $result;
    }
}
