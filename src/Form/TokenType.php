<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

/**
 * @author mati@we.ee
 */
class TokenType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        parent::buildForm($builder, $options);

        $builder->add('grant_type', ChoiceType::class, [
            'constraints' => [
                new NotBlank()
            ],
            'choices' => [
                'authorization_code' => 'authorization_code'
            ]
        ]);

        $builder->add('code', TextType::class, [
            'constraints' => [
                new NotBlank(),
            ]
        ]);
        $builder->add('redirect_uri', UrlType::class, [
            'constraints' => [
                new NotBlank(),
                new Url()
            ]
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('csrf_protection', false);
    }
}