<?php
namespace App\Form\Toggle;

use Closure;
use LogicException;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\ParsedExpression;

/**
 *
 * @author mati@we.ee
 */
final class ToggleConfig
{

    /**
     *
     * @var type 
     */
    private $id;

    /**
     *
     * @var string 
     */
    private $expression;

    /**
     * 
     * @var TogglerCollection 
     */
    private $togglers;

    /**
     *
     * @var ParsedExpression 
     */
    private $parsedExpression;

    public function __construct(string $expression = null, array $togglers = [], string $id = null)
    {
        $args = [];
        foreach ($togglers as $toggler) {
            $this->getTogglers()->add($toggler);
            $args[] = $toggler->getVar();
        }
        if ($expression) {
            $this->expression = vsprintf($expression, $args);
        }
        if ($id) {
            $this->id = $id;
        }
    }

    public function getTogglers(): TogglerCollection
    {
        if (!$this->togglers) {
            $this->togglers = new TogglerCollection();
        }
        return $this->togglers;
    }

    public function isToggled(): bool
    {
        return (bool) $this->expression;
    }

    /**
     * 
     * @return ParsedExpression|null
     */
    public function getParsedExpression()
    {

        if (!$this->parsedExpression) {
            $language = new ExpressionLanguage();
            foreach ($this->getFunctions() as $name => $data) {
                $language->register($name, $data['compiler'], $data['evaluator']);
            }

            $this->parsedExpression = $language->parse($this->expression, $this->getTogglers()->getVars());
        }
        return $this->parsedExpression;
    }

    public function getFunctions(): array
    {
        return [
            'collection_set_values' => [
                'compiler' => function ($collection, $setKey) {

                    return sprintf('collection_set_values(%s, %s)', $collection, $setKey);
                },
                'evaluator' => function (array $values, array $collection = null, string $setKey): array {
                    if (!$collection) {
                        return [];
                    }
                    $return = [];
                    foreach ($collection as $i => $set) {
                        if (!is_array($set)) {
                            throw new LogicException('expected set values as array');
                        }
                        if ($set[$setKey] and !in_array($set[$setKey], $return)) {
                            $return[] = $set[$setKey];
                        }

                    }

                    return $return;
                }
            ]
        ];
    }

    public function getId()
    {
        if (!$this->id) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function isToggler(): bool
    {
        return (bool) $this->id;
    }

    public static function createWithId(string $id, string $expression = null, array $togglers = []): self
    {
        $r = new self($expression, $togglers);
        $r->id = $id;
        return $r;
    }

    public function createToggler(string $var = 'value', Closure $value_resolver = null): Toggler
    {
        return new Toggler($this, $var, $value_resolver);
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }
}
