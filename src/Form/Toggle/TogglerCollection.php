<?php
namespace App\Form\Toggle;

use ArrayIterator;
use IteratorAggregate;
use LogicException;

/**
 * @author mati@we.ee
 */
final class TogglerCollection implements IteratorAggregate
{

    /**
     *
     * @var bool 
     */
    private $locked;

    /**
     *
     * @var Toggler[] 
     */
    protected $data = [];

    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    public function add(Toggler $item)
    {
        if ($this->locked) {
            throw new LogicException('Form TogglerCollection already locked');
        }
        foreach ($this->data as $value) {
            if ($value->getConfig()->getId() === $item->getConfig()->getId()) {
                throw new LogicException('Toggler with id "' . $item->getId() . '" exists');
            }
            if ($value->getVar() === $item->getVar()) {
                throw new LogicException('Toggler with var "' . $item->getConfig()->getVar() . '" exists');
            }
        }
        $this->data[] = $item;
    }

    /**
     * 
     * @throws LogicException when locked
     */
    public function lock()
    {
        $this->locked = true;
    }

    public function hasConfig(ToggleConfig $item): bool
    {
        foreach ($this->data as $value) {
            if ($value->getConfig()->getId() === $item->getId()) {
                return true;
            }
        }
        return false;
    }

    public function getVars(): array
    {
        $r = [];
        foreach ($this->data as $item) {
            $r[] = $item->getVar();
        }
        return $r;
    }

    public function getJsConfig(): array
    {
        $r = [];
        foreach ($this->data as $item) {
            $r[] = [
                'id' => $item->getConfig()->getId(),
                'var' => $item->getVar(),
            ];
        }
        return $r;
    }
}
