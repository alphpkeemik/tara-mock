<?php
namespace App\Form\Toggle;

use Closure;
use Symfony\Component\Form\FormInterface;

/**
 * @author mati@we.ee
 */
final class Toggler
{

    /**
     * Toggle config
     * @var ToggleConfig 
     */
    private $toggle_config;

    /**
     * Var
     * @var string 
     */
    private $var;

    /**
     * Value resolver
     * @var Closure 
     */
    private $value_resolver;

    /**
     * Default value resolver
     * @var Closure 
     */
    private static $default_value_resolver;

    public function __construct(ToggleConfig $toggle_config, string $var, Closure $value_resolver = null)
    {
        $this->toggle_config = $toggle_config;
        $this->var = $var;

        $this->value_resolver = $value_resolver;
        // auto generate id if needed
        $toggle_config->getId();
    }

    /**
     * get Toggle config
     * @return ToggleConfig
     */
    public function getConfig(): ToggleConfig
    {
        return $this->toggle_config;
    }

    /**
     * get Var
     * @return string
     */
    public function getVar(): string
    {
        return $this->var;
    }

    /**
     * get Value resolver
     * @return Closure
     */
    public function getValue(FormInterface $toggler = null)
    {
        $value_resolver = $this->value_resolver;
        if (!$value_resolver) {
            if (!static::$default_value_resolver) {
                static::$default_value_resolver = function(FormInterface $toggler = null) {
                    if (!$toggler) {
                        return null;
                    }
                    $options = $toggler->getConfig()->getOptions();
                    if (key_exists('multiple', $options) && $options['multiple'] and ! is_array($toggler->getData())) {
                        return [];
                    }
                    return $toggler->getData();
                };
            }
            $value_resolver = static::$default_value_resolver;
        }
        return $value_resolver($toggler);
    }
}
