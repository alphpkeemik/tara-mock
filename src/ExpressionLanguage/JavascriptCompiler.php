<?php

namespace App\ExpressionLanguage;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\Node;

/**
 * @author mati@we.ee
 */
class JavascriptCompiler extends Compiler
{

    private $source;

    /**
     * Gets the current Javascript code after compilation.
     *
     * @return string The Javascript code
     */
    public function getSource()
    {
        return $this->source;
    }

    public function reset()
    {
        $this->source = '';

        return $this;
    }

    /**
     * Compiles a node.
     *
     * @param Node $node The node to compile
     *
     * @return Compiler The current compiler instance
     */
    public function compile(Node $node)
    {
        $this->compileNode($node);

        return $this;
    }

    public function subcompile(Node $node)
    {
        $current = $this->source;
        $this->source = '';

        $this->compileNode($node);

        $source = $this->source;
        $this->source = $current;

        return $source;
    }

    private function compileNode(Node $node)
    {
        $repr = 'App\ExpressionLanguage\JavascriptCompilerNode\\' . str_replace('Symfony\Component\ExpressionLanguage\Node\\', '', get_class($node));
        if (class_exists($repr)) {
            $nc = new $repr($node);
            $nc->compile($this);
        } else {
            $node->compile($this);
        }
    }

    /**
     * Adds a raw string to the compiled code.
     *
     * @param string $string The string
     *
     * @return Compiler The current compiler instance
     */
    public function raw($string)
    {
        $this->source .= $string;

        return $this;
    }

    /**
     * Adds a quoted string to the compiled code.
     *
     * @param string $value The string
     *
     * @return Compiler The current compiler instance
     */
    public function string($value)
    {
        $this->source .= sprintf('"%s"', addcslashes($value, "\0\t\"\$\\"));

        return $this;
    }

    /**
     * Returns a Javascript representation of a given value.
     *
     * @param mixed $value The value to convert
     *
     * @return Compiler The current compiler instance
     */
    public function repr($value)
    {
        if (is_int($value) || is_float($value)) {
            if (false !== $locale = setlocale(LC_NUMERIC, 0)) {
                setlocale(LC_NUMERIC, 'C');
            }

            $this->raw($value);

            if (false !== $locale) {
                setlocale(LC_NUMERIC, $locale);
            }
        } elseif (null === $value) {
            $this->raw('null');
        } elseif (is_bool($value)) {
            $this->raw($value ? 'true' : 'false');
        } elseif (is_array($value)) {
            $this->raw(json_encode($value));
        } else {
            $this->string($value);
        }

        return $this;
    }
}
