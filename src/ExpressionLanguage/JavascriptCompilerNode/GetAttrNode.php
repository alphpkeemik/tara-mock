<?php

namespace App\ExpressionLanguage\JavascriptCompilerNode;

use Symfony\Component\ExpressionLanguage\Compiler;

class GetAttrNode extends BaseNode
{

    const PROPERTY_CALL = 1;
    const METHOD_CALL = 2;
    const ARRAY_CALL = 3;

    public function compile(Compiler $compiler)
    {
        switch ($this->node->attributes['type']) {
            case self::PROPERTY_CALL:
                $compiler
                    ->compile($this->node->nodes['node'])
                    ->raw('.')
                    ->raw($this->node->nodes['attribute']->attributes['value'])
                ;
                break;

            case self::METHOD_CALL:
                $compiler
                    ->compile($this->node->nodes['node'])
                    ->raw('.')
                    ->raw($this->node->nodes['attribute']->attributes['value'])
                    ->raw('(')
                    ->compile($this->node->nodes['arguments'])
                    ->raw(')')
                ;
                break;

            case self::ARRAY_CALL:
                $compiler
                    ->compile($this->node->nodes['node'])
                    ->raw('[')
                    ->compile($this->node->nodes['attribute'])->raw(']')
                ;
                break;
        }
    }
}
