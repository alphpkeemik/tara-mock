<?php

namespace App\ExpressionLanguage\JavascriptCompilerNode;

use Symfony\Component\ExpressionLanguage\Compiler;

class NameNode extends BaseNode
{

    public function compile(Compiler $compiler)
    {
        $compiler->raw($this->node->attributes['name']);
    }
}
