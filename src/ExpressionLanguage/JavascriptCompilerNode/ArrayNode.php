<?php

namespace App\ExpressionLanguage\JavascriptCompilerNode;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\ConstantNode;

class ArrayNode extends BaseNode
{

    public function compile(Compiler $compiler)
    {

        $withKeys = false;
        $last = -1;
        foreach ($this->getKeyValuePairs() as $pair) {
            $key = $pair['key'];
            if ($key instanceof ConstantNode) {
                $key = $key->attributes['value'];
            }
            if (!is_numeric($key)) {
                $withKeys = true;
                break;
            }
            if ($last + 1 != $key) {
                $withKeys = true;
                break;
            }
            $last = $key;
        }
        $compiler->raw($withKeys ? '{' : '[');
        $this->compileArguments($compiler, $withKeys);
        $compiler->raw($withKeys ? '}' : ']');
    }

    protected function getKeyValuePairs()
    {
        $pairs = array();
        foreach (array_chunk($this->node->nodes, 2) as $pair) {
            $pairs[] = array('key' => $pair[0], 'value' => $pair[1]);
        }

        return $pairs;
    }

    protected function compileArguments(Compiler $compiler, $withKeys = true)
    {
        $first = true;
        foreach ($this->getKeyValuePairs() as $pair) {
            if (!$first) {
                $compiler->raw(', ');
            }
            $first = false;

            if ($withKeys) {
                $compiler
                    ->compile($pair['key'])
                    ->raw(' : ')
                ;
            }

            $compiler->compile($pair['value']);
        }
    }
}
