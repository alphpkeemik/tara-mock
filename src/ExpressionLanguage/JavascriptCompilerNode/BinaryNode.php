<?php

namespace App\ExpressionLanguage\JavascriptCompilerNode;

use Symfony\Component\ExpressionLanguage\Compiler;

class BinaryNode extends BaseNode
{

    private static $operators = array(
        '~' => '+',
        'and' => '&&',
        'or' => '||',
    );
    private static $functions = array(
        '**' => 'pow',
        '..' => 'range',
        'in' => 'in_array',
        'not in' => '!in_array',
    );

    public function compile(Compiler $compiler)
    {
        $operator = $this->node->attributes['operator'];

        if ('matches' == $operator) {
            $compiler
                ->raw('preg_match(')
                ->compile($this->node->nodes['right'])
                ->raw(', ')
                ->compile($this->node->nodes['left'])
                ->raw(')')
            ;

            return;
        }

        if (isset(self::$functions[$operator])) {
            $compiler
                ->raw(sprintf('%s(', self::$functions[$operator]))
                ->compile($this->node->nodes['left'])
                ->raw(', ')
                ->compile($this->node->nodes['right'])
                ->raw(')')
            ;

            return;
        }

        if (isset(self::$operators[$operator])) {
            $operator = self::$operators[$operator];
        }

        $compiler
            ->raw('(')
            ->compile($this->node->nodes['left'])
            ->raw(' ')
            ->raw($operator)
            ->raw(' ')
            ->compile($this->node->nodes['right'])
            ->raw(')')
        ;
    }
}
