<?php

namespace App\ExpressionLanguage\JavascriptCompilerNode;

use Symfony\Component\ExpressionLanguage\Compiler;
use Symfony\Component\ExpressionLanguage\Node\Node;

abstract class BaseNode
{

    /**
     *
     * @var Node 
     */
    protected $node;

    public function __construct(Node $node)
    {
        $this->node = $node;
    }

    public abstract function compile(Compiler $compiler);
}
