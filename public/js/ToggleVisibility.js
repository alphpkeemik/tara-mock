/* global AppPolyfill */
/**
 *
 * @param {Node} node
 * @param {Node} context
 * @returns {AppToggleVisibility}
 * @property {Array} togglers
 * @property {String} expression
 * @property {String} toogle_attr  for container
 * @property {String} disabled_dataset_key for input
 * @property {String} source_attr for source finding
 */
function AppToggleVisibility(node, context) {
    this._node = node;
    this._context = context;
    this._force = false;
    this._reference = [];
    this._observer = null;

    this.togglers = [];
    this.expression = null;
    this.toogle_attr = 'data-app-togglevisibility-hidden';
    this.disabled_dataset_key = 'toggle_visibility_disabled';
    this.source_attr = 'data-app-togglevisibility-source';
}
AppToggleVisibility.events = {};
AppToggleVisibility.events.enabled = 'AppToggleVisibility_enabled';
AppToggleVisibility.events.predisable = 'AppToggleVisibility_predisable';
AppToggleVisibility.events.disabled = 'AppToggleVisibility_disabled';
AppToggleVisibility.prototype.expressions = {
    pow: Math.pow,
    preg_match: function(pattern, subject) {
        return pattern.exec(subject);
    },
    in_array: function(needle, haystack) {
        return haystack.indexOf(needle) > -1;
    },
    range: function(low, high, step) {
        //  discuss at: http://locutus.io/php/range/     
        var matrix = [];
        var iVal;
        var endval;
        var plus;
        var walker = step || 1;
        var chars = false;

        if (!isNaN(low) && !isNaN(high)) {
            iVal = low;
            endval = high;
        } else if (isNaN(low) && isNaN(high)) {
            chars = true;
            iVal = low.charCodeAt(0);
            endval = high.charCodeAt(0);
        } else {
            iVal = (isNaN(low) ? 0 : low);
            endval = (isNaN(high) ? 0 : high);
        }

        plus = !(iVal > endval);
        if (plus) {
            while(iVal <= endval) {
                matrix.push(((chars) ? String.fromCharCode(iVal) : iVal));
                iVal += walker;
            }
        } else {
            while(iVal >= endval) {
                matrix.push(((chars) ? String.fromCharCode(iVal) : iVal));
                iVal -= walker;
            }
        }
        return matrix;
    },
    collection_set_values: function (collection, setkey) {
        if (!collection) {
            return [];
        }
        var r = [];
        for (var i = 0, l = collection.length; i < l; i++) {
            if (collection[i][setkey] &&
                r.indexOf(collection[i][setkey]) === -1
            ) {
                r.push(collection[i][setkey]);
            }
        }
        return r;
    }
};

AppToggleVisibility.prototype.startListening = function() {
    this._observer = new MutationObserver(this._handle.bind(this));
    this._observer.observe(this._context, {subtree: true, childList: true});
    this._do_handle(true);
};
AppToggleVisibility.prototype._handle = function() {
    this._do_handle(false);
};
AppToggleVisibility.prototype._do_handle = function(updated) {

    var els = [], i, other = [], index, el, handler, handler, refs = [], val;
    for(i in this.togglers) {
        el = this._context.querySelector('[' + this.source_attr + '="' + this.togglers[i] + '"]');
        if (el) {
            els.push(el);
            refs.push({node: el, id: this.togglers[i], 'var': i});
        }
    }
    for(i = 0; this._reference[i]; i++) {
        if ((index = els.indexOf(this._reference[i].node)) !== -1) {
            val = this._getValue(this._reference[i].node);
            if (val !== this._reference[i].val) {
                this._reference[i].val = val;
                this._execute();
            }

            els.splice(index, 1);
        } else {
            other.push(this._reference[i]);
        }
    }
    for(i = 0; refs[i]; i++) {
        if (els.indexOf(refs[i].node) === -1) {
            continue;
        }
        handler = this._handle_node.bind(this, refs[i].node);
        refs[i].node.addEventListener('change', handler);
        refs[i].node.addEventListener(AppToggleVisibility.events.enabled, handler);
        refs[i].node.addEventListener(AppToggleVisibility.events.predisable, handler);
        refs[i].handler = handler;
        refs[i].val = this._getValue(refs[i].node);
        this._reference.push(refs[i]);

    }
    for(i = 0; other[i]; i++) {
        this._reference.splice(this._reference.indexOf(other[i]), 1);
        other[i].node.removeEventListener('change', other[i].handler);
        other[i].node.removeEventListener(AppToggleVisibility.events.enabled, other[i].handler);
        other[i].node.removeEventListener(AppToggleVisibility.events.predisable, other[i].handler);
        updated = true;
    }
    if (updated) {
        this._execute();
    }
};
AppToggleVisibility.prototype._handle_node = function(node, e) {
    for(var i = 0; this._reference[i]; i++) {
        if (node === this._reference[i].node) {
            var val = e.type === AppToggleVisibility.events.predisable ? null : this._getValue(node);
            if (val !== this._reference[i].val) {
                this._reference[i].val = val;
                this._execute();
            }
        }
    }
};
AppToggleVisibility.prototype._getValue = function(node) {
    if (node.disabled || node.hasAttribute('disabled')) {
        // @todo
        // check why installation address will be not disabled when
        // seems tah AppToggleVisibility.events.enabled will be not handled
        // no time to debug more atm
  //      return null;
    }
    return this.resolveValue(node);
};
AppToggleVisibility.prototype._execute = function() {
    if (this._evaluate()) {
        if (this._node.hasAttribute(this.toogle_attr)) {
            this._node.removeAttribute(this.toogle_attr);
            var nodes = this._node.querySelectorAll('[name]'), i = 0;
            for(; nodes[i]; i++) {
                if (!nodes[i].disabled || !nodes[i].dataset[this.disabled_dataset_key]) {
                    continue;
                }
                nodes[i].disabled = false;
                nodes[i].dispatchEvent(AppPolyfill.CustomEvent(AppToggleVisibility.events.enabled, {bubbles: true}));
            }
            nodes = this._node.querySelectorAll('[disabled]');
            for (i = 0; nodes[i]; i++) {
                nodes[i].removeAttribute('disabled');
            }

        }
    } else {
        if (!this._node.hasAttribute(this.toogle_attr)) {
            this._node.setAttribute(this.toogle_attr, '');
            var nodes = this._node.querySelectorAll('[name]'), i = 0;
            for(; nodes[i]; i++) {
                if (nodes[i].disabled) {
                    continue;
                }
                nodes[i].dataset[this.disabled_dataset_key] = true;

                // fire before https://stackoverflow.com/questions/3100319/event-on-a-disabled-input
                nodes[i].dispatchEvent(AppPolyfill.CustomEvent(AppToggleVisibility.events.predisable, {bubbles: true}));

                nodes[i].disabled = true;
                nodes[i].parentNode.dispatchEvent(AppPolyfill.CustomEvent(AppToggleVisibility.events.disabled, {bubbles: true}));
            }
        }
    }
};


AppToggleVisibility.prototype._evaluate = function() {
    var code = ["'use strict'"];
    var values = {};
    for(var v in this.togglers) {
        values[v] = null;
        code.push('var ' + v+ '=values["' + v + '"]');
    }
    for(var i = 0; this._reference[i]; i++) {
        values[this._reference[i].var] = this._reference[i].val;
    }
    for(var n in this.expressions) {
        code.push('var ' + n + '=expressions["' + n + '"]');
    }
    code.push('return ' + this.expression + ';');
    var executer = new Function('values', 'expressions', code.join(';'));
    try {
        var r = executer(values, this.expressions);
        return r;
    } catch(e) {
        console.log(values, this._reference);
        if (window.console) {
            console.error('AppToggleVisibility error evaluationg expession "%s" for value "%s"', this.expression, this._lastVal);
            console.debug(e);
        }
        return true;
    }
};
AppToggleVisibility.prototype.resolveCollectionSetValue = function (node, id) {
    var dk = 'appTogglevisibilityCollection_child_' + id;
    var els = node.querySelectorAll('[data-app-togglevisibility-collection_child_' + id + ']');
    var value = {}, i;
    for (i = 0; els[i]; i++) {
        value[els[i].dataset[dk]] = this.resolveValue(els[i]);
    }
    return value;
};
AppToggleVisibility.prototype.resolveCollectionValue = function (node) {
    var els = node.querySelectorAll(node.dataset.appTogglevisibilityCollection_set_selector);
    if (!els.length) {
        // while in php (![])==false but in js true
        return null;
    }
    var value = [], i;
    for (i = 0; els[i]; i++) {
        value.push(this.resolveCollectionSetValue(els[i], node.dataset.appTogglevisibilityCollection_id));
    }
    return value;
};
AppToggleVisibility.prototype.resolveValue = function(node) {
    var ud;
    if (node.dataset.appTogglevisibilityCollection_id) {
        return this.resolveCollectionValue(node);
    }

    if (node.nodeName) {
        var nn = node.nodeName.toLowerCase();

        // handle single checkbox
        if (nn === 'input' && node.type === 'checkbox') {
            return node.checked ? (node.value ? node.value : true) : null;
        }
        // handle multi select
        if (nn === 'select' && node.multiple) {
            var value = [];
            for(var els = node.getElementsByTagName("option"), i = 0; els[i]; i++)
            {
                if (els[i].selected) {
                    value.push(els[i].value);
                }
            }
            return value;
        }
        if (node.querySelectorAll) {
            var els = node.querySelectorAll('[name]'), i;

            if (els[0] && els[0].nodeName) {
                var cnn = els[0].nodeName.toLowerCase();
                // handle mulitiple checkboxes
                if (cnn === 'input' && els[0].type === 'checkbox') {
                    var value = [], i;
                    for(i = 0; els[i]; i++) {
                        if (els[i].checked) {
                            value.push(els[i].value);
                        }
                    }
                    return value;
                }
                // handle radio
                if (cnn === 'input' && els[0].type === 'radio') {
                    for(i = 0; els[i]; i++) {
                        if (els[i].checked) {
                            return els[i].value;
                        }
                    }
                }
            }
        }
    }
    return node.value === ud ? '' : node.value;
};
