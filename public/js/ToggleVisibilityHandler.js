;
document.addEventListener("DOMContentLoaded", function(event) {
    var els = document.querySelectorAll('[data-app-form-toggle_visibility]');
    for(var i = 0, ss, dataAttr; els[i]; i++) {
        ss = new AppToggleVisibility(els[i], document);
        dataAttr = els[i].getAttribute('data-app-form-toggle_visibility');
        var data = JSON.parse(dataAttr.replace(/\n/g, '\\n'));
        for(var n in data) {
            ss[n] = data[n];
        }
        ss.startListening();
    }
});
