"use strict";

function AppPolyfill() {
}

AppPolyfill.matchesSelector = (function() {
    var matchesFn, rc = function() {
        // find vendor prefix
        ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
            if (typeof document.body[fn] === 'function') {
                matchesFn = fn;
                return true;
            }
            return false;
        });
    };
    return function(el, selector) {
        if (!matchesFn) {
            rc();
        }
        return el[matchesFn](selector);
    };
})();
AppPolyfill.closestSelector = (function() {
    var c = 0, closest, rc = function() {
        if ('closest' in document.body && typeof document.body.closest === 'function') {
            c = 1;
        } else {
            closest = function(el, selector) {
                // traverse parents
                var parent;
                while (el !== null) {
                    parent = el.parentElement;
                    if (parent !== null && AppPolyfill.matchesSelector(parent, selector)) {
                        return parent;
                    }
                    el = parent;
                }
                return null;
            };
        }
    };
    return function(el, selector) {
        var r;
        if (!c) {
            rc();
        }
        if (c === 1) {
            r = el.closest(selector);
        } else {
            r = closest(el, selector);
        }
        return r;
    };
})();
/**
 * https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent
 */
AppPolyfill.CustomEvent = (function() {
    if (typeof window.CustomEvent === "function") {
        return function(event, params) {
            return new window.CustomEvent(event, params);
        };
    }
    var CustomEvent = function(event, params) {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    };
    CustomEvent.prototype = window.Event.prototype;
    return CustomEvent;
})();